{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

dbName = "blog.db" 

data Message = Message {
    author :: T.Text,
    title :: T.text,
    body :: T.text
}

instance FromRow Message where
    fromRow = Message <$> field <*> field <*> field

selectMessage :: IO [Message]
selectMessage = do
    conn <- SQL.open dbName
    res <- DSL.query_ conn "SELECT author, title, body, FROM messages"
        :: IO [Message]
    SQL.close conn
    return res