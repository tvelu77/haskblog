{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules #-}

module View 
(
    homeRoute,
    writeRoute
) where

import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid

import qualified Model

homeRoute :: [Model.Message] -> L.Text
homeRoute messages = renderText $ do
    doctype_
    html_ $ body_ $ do
        