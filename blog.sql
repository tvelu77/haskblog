CREATE TABLE messages (
    id INTEGER PRIMARY KEY,
    author TEXT,
    title TEXT,
    body TEXT
);

INSERT INTO messages VALUES (
    0,
    "Testor",
    "This is a test !",
    "Hello everybody, if you see this message, congratulations :)"
);